<?php
class LinksController extends AppController{

  public function index(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Links';
    $this->subtitulo = 'Listado';
    $this->url = 'links';
    $this->listLinks = (new Links())->getLinks();
  }

  public function create()
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Link';
    $this->subtitulo = 'Crear';
    $this->url = 'links';

    if(Input::hasPost('links')){
      $link = new Links(Input::post('links'));
      if($link->create()){
        Flash::valid('Operación exitosa');
        Input::delete();
        return;
      }
      Flash::error('Fallo la operación');
    }
  }

  public function update($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Links';
    $this->subtitulo = 'Editar';
    $this->url = 'links';

    $link = new Links();
    if(Input::hasPost('link')){
      $data_link = array(
        'id' =>Input::post('link')['id'],
        'titulo' =>Input::post('link')['titulo'],
        'link' =>Input::post('link')['link']
      );
      if($link->update($data_link)){
        Flash::valid('Operación exitosa');
        return Redirect::to();
      }
      Flash::error('Fallo la operación');
      return;
    }
    $this->link = $link->find_by_id((int) $id);
  }

  public function delete($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    if ((new Links)->delete((int) $id)) {
            Flash::valid('Operación exitosa');
    } else {
            Flash::error('Falló Operación');
    }
    return Redirect::to();
  }

}
