<?php

class CategoriaspostController extends AppController
{
  public function index(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Categorias de Posts';
    $this->subtitulo = 'Listado';
    $this->url = 'categoriaspost';
    $categoria = new Categoriaspost();
    $this->listCategorias = $categoria->getCategorias();
  }

  public function create()
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Categoria';
    $this->subtitulo = 'Crear';
    $this->url = 'categoriaspost';

    if(Input::hasPost('categoriaspost')){
      $categoria = new Categoriaspost(Input::post('categoriaspost'));
      if($categoria->create()){
        Flash::valid('Operación exitosa');
        Input::delete();
        return;
      }
      Flash::error('Fallo la operación');
    }
  }

  public function update($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Categorias';
    $this->subtitulo = 'Editar';
    $this->url = 'categoriaspost';

    $categoria = new Categoriaspost();
    if(Input::hasPost('categoriapost')){
      $data_categoria = array(
        'id' =>Input::post('categoriapost')['id'],
        'nombre' =>Input::post('categoriapost')['nombre']
      );
      if($categoria->update($data_categoria)){
        Flash::valid('Operación exitosa');
        return Redirect::to();
      }
      Flash::error('Fallo la operación');
      return;
    }
    $this->categoriapost = $categoria->find_by_id((int) $id);
  }

  public function delete($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    if ((new Categoriaspost)->delete((int) $id)) {
            Flash::valid('Operación exitosa');
    } else {
            Flash::error('Falló Operación');
    }
    return Redirect::to();
  }
}
