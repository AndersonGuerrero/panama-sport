<?php

class CategoriasController extends AppController
{
  public function index(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Categorias de Noticias';
    $this->subtitulo = 'Listado';
    $this->url = 'categorias';
    $categoria = new Categorias();
    $this->listCategorias = $categoria->getCategorias();
  }

  public function create()
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Categoria';
    $this->subtitulo = 'Crear';
    $this->url = 'categorias';

    if(Input::hasPost('categorias')){
      $categoria = new Categorias(Input::post('categorias'));
      if($categoria->create()){
        Flash::valid('Operación exitosa');
        Input::delete();
        return;
      }
      Flash::error('Fallo la operación');
    }
  }

  public function update($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Categorias';
    $this->subtitulo = 'Editar';
    $this->url = 'categorias';

    $categoria = new Categorias();
    if(Input::hasPost('categoria')){
      $data_categoria = array(
        'id' =>Input::post('categoria')['id'],
        'nombre' =>Input::post('categoria')['nombre']
      );
      if($categoria->update($data_categoria)){
        Flash::valid('Operación exitosa');
        return Redirect::to();
      }
      Flash::error('Fallo la operación');
      return;
    }
    $this->categoria = $categoria->find_by_id((int) $id);
  }

  public function delete($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    if ((new Categorias)->delete((int) $id)) {
            Flash::valid('Operación exitosa');
    } else {
            Flash::error('Falló Operación');
    }
    return Redirect::to();
  }
}
