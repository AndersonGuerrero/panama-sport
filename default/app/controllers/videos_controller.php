<?php
class VideosController extends AppController{

  public function index(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Videos';
    $this->subtitulo = 'Listado';
    $this->url = 'videos';
    $this->listVideos = (new Videos())->getVideos();
  }

  public function create()
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Videos';
    $this->subtitulo = 'Crear';
    $this->url = 'videos';

    if(Input::hasPost('videos')){
      $video = new Videos(Input::post('videos'));
      if($video->create()){
        Flash::valid('Operación exitosa');
        Input::delete();
        return;
      }
      Flash::error('Fallo la operación');
    }
  }

  public function update($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Videos';
    $this->subtitulo = 'Editar';
    $this->url = 'videos';

    $video = new Videos();
    if(Input::hasPost('video')){
      $data_video = array(
        'id' =>Input::post('video')['id'],
        'titulo' =>Input::post('video')['titulo'],
        'link' =>Input::post('video')['link']
      );
      if($video->update($data_video)){
        Flash::valid('Operación exitosa');
        return Redirect::to();
      }
      Flash::error('Fallo la operación');
      return;
    }
    $this->video = $video->find_by_id((int) $id);
  }

  public function delete($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    if ((new Videos)->delete((int) $id)) {
            Flash::valid('Operación exitosa');
    } else {
            Flash::error('Falló Operación');
    }
    return Redirect::to();
  }

}
