<?php
class AdminController extends AppController
{

    public function index()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin/login');
      }
      View::template('admin');
      $this->titulo = 'Panel de control';
      $this->url = 'index';
      $this->subtitulo = '...';
      $this->banner_count = (new Banners())->count();
      $this->noticias_count = (new Noticias())->count();
      $this->noticias_carrusel = (new Noticias())->count("conditions: carrusel=1");
      $this->post_count = (new Posts())->count();
      $this->links_count = (new Links())->count();
      $this->videos_count = (new Videos)->count();
    }

    public function logout(){
      Auth::destroy_identity();
      // Auth::destroy_active_session();
      return Redirect::to('admin/login');
    }

    public function login()
    {
      if(Auth::is_valid()){
        return Redirect::to('admin');
      }
      View::template('login');
      $this->titulo = 'Login';
      $this->subtitulo = 'Admin';
      if(Input::hasPost('usuarios')){
        $pwd = Input::post('usuarios.password');
        $correo = Input::post('usuarios.correo');
        $auth = new Auth("model", "class: usuarios", "correo: $correo", "password: $pwd");
        if ($auth->authenticate()) {
          return Redirect::to("admin");
        }else{
          Flash::error('Correo o contraseña incorrecta');
        }
      }
    }
}
