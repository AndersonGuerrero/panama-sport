<?php
class WebController extends CustomwebController
{
    public function noticia($id){
      $this->noticia = (new Noticias())->find_by_id((int) $id);
      $this->noticia->vistas++;
      $this->noticia->save();
      $this->categoria = (new Categorias())->find_by_id(
        (int) $this->noticia->categorias_id);
      $this->noticiasRelacionadas = (new Noticias())->find(
        "limit: 3",
        "order: created_at desc",
        "conditions: categorias_id="
        .$this->noticia->categorias_id." and not id=".$this->noticia->id
      );
    }

    public function noticias($id){
      $categoria = (new Categorias())->find_by_id((int) $id);
      $this->categoria = $categoria;
      $this->noticias = Functions::ordenarLista($this->noticias);
    }

    public function posts($id){
      $this->categoria = (new Categoriaspost())->find_by_id((int) $id);
      $posts = (new Posts())->find(
        "conditions: categoriaspost_id=".$this->categoria->id
      );
      $this->posts = Functions::ordenarLista($posts);
    }

    public function post($id){
      $this->post = (new Posts())->find_by_id((int) $id);
      $this->post->vistas++;
      $this->post->save();
      $categoriaspost_id = $this->post->categoriaspost_id;
      $this->categoria = (new Categoriaspost())->find_by_id((int) $categoriaspost_id);
      $this->postsRelacionados = (new Posts())->find(
        "limit: 3",
        "order: created_at desc",
        "conditions: categoriaspost_id="
        .$this->post->categoriaspost_id." and not id=".$this->post->id
      );
    }

    public function contacto(){

    }

    public function index(){
      $this->noticias = Functions::ordenarLista($this->noticias);
      $this->url = 'web/index';
      $this->listNoticiasCarrusel = (new Noticias())->find(
        "per_page: 10",
        "conditions: carrusel",
        "order: created_at desc"
      );
    }
}
