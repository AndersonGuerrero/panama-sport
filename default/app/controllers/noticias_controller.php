<?php

class NoticiasController extends AppController
{
  public function index(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Noticias';
    $this->subtitulo = 'Listado';
    $this->url = 'noticias';
    $noticia = new Noticias();
    $this->listNoticias = (new Noticias())->find();
  }

  public function create()
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Noticias';
    $this->subtitulo = 'Crear';
    $this->url = 'noticias';

    if(Input::hasPost('noticias')){
      if(Input::post('carrusel')){
        $carrusel = 1;
      }else{
        $carrusel = 0;
      }
      $data_noticia = array(
        'contenido' =>Input::post('noticias')['contenido'],
        'titulo' =>Input::post('noticias')['titulo'],
        'carrusel' => $carrusel,
        'vistas' => 0,
        'categorias_id' =>Input::post('noticias')['categorias_id']
      );
      if($_FILES){
        $target_path = FILES_PATH.'noticias/';
        $target_path = $target_path.basename($_FILES['imagen']['name']);
        if(move_uploaded_file($_FILES['imagen']['tmp_name'], $target_path)){
          $data_noticia['imagen'] = $_FILES['imagen']['name'];
        }
      }

      $noticia = new Noticias($data_noticia);

      if($noticia->create()){
        Flash::valid('Operación exitosa');
        Input::delete();
        return;
      }
      Flash::error('Fallo la operación');
    }
  }

  public function update($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Noticias';
    $this->subtitulo = 'Editar';
    $this->url = 'noticias';

    $noticia = new Noticias();
    if(Input::hasPost('noticia')){
      if(Input::post('carrusel')){
        $carrusel = 1;
      }else{
        $carrusel = 0;
      }
      $data_noticia = array(
        'id' =>Input::post('noticia')['id'],
        'titulo' =>Input::post('noticia')['titulo'],
        'contenido' =>Input::post('noticia')['contenido'],
        'carrusel' => $carrusel,
        'categorias_id' =>Input::post('noticia')['categorias_id']
      );
      if($_FILES){
        $target_path = FILES_PATH.'noticias/';
        $target_path = $target_path.basename($_FILES['imagen']['name']);
        if(move_uploaded_file($_FILES['imagen']['tmp_name'], $target_path)){
          $data_noticia['imagen'] = $_FILES['imagen']['name'];
        }
      }
      if($noticia->update($data_noticia)){
        Flash::valid('Operación exitosa');
        return Redirect::to();
      }
      Flash::error('Fallo la operación');
      return;
    }
    $this->noticia = $noticia->find_by_id((int) $id);
  }

  public function delete($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    if ((new Noticias)->delete((int) $id)) {
            Flash::valid('Operación exitosa');
    } else {
            Flash::error('Falló Operación');
    }
    return Redirect::to();
  }
}
