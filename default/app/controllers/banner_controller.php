<?php

class BannerController extends AppController
{
  public function index(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Banners';
    $this->subtitulo = 'Listado';
    $this->url = 'banner';
    $banner = new Banners();
    $this->listBanners = $banner->getBanners();
  }

  public function create()
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Banners';
    $this->subtitulo = 'Crear';
    $this->url = 'banner';

    if(Input::hasPost('banners')){
      $data_banner = array(
        'link' =>Input::post('banners')['link']
      );
      if($_FILES){
        $target_path = FILES_PATH.'banners/';
        $target_path = $target_path.basename($_FILES['imagen']['name']);
        if(move_uploaded_file($_FILES['imagen']['tmp_name'], $target_path)){
          $data_banner['imagen'] = $_FILES['imagen']['name'];
        }
      }
      $banner = new Banners($data_banner);
      if($banner->create()){
        Flash::valid('Operación exitosa');
        Input::delete();
        return;
      }
      Flash::error('Fallo la operación');
    }
  }

  public function update($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Banners';
    $this->subtitulo = 'Editar';
    $this->url = 'banner';

    $banner = new Banners();
    if(Input::hasPost('banners')){
      $data_banner = array(
        'id' =>Input::post('banners')['id'],
        'link' =>Input::post('banners')['link']
      );
      if($_FILES){
        $target_path = FILES_PATH.'banners/';
        $target_path = $target_path.basename($_FILES['imagen']['name']);
        if(move_uploaded_file($_FILES['imagen']['tmp_name'], $target_path)){
          $data_banner['imagen'] = $_FILES['imagen']['name'];
        }
      }
      if($banner->update($data_banner)){
        Flash::valid('Operación exitosa');
        return Redirect::to();
      }
      Flash::error('Fallo la operación');
      return;
    }
    $this->banners = $banner->find_by_id((int) $id);
  }

  public function delete($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    if ((new Banners)->delete((int) $id)) {
            Flash::valid('Operación exitosa');
    } else {
            Flash::error('Falló Operación');
    }
    return Redirect::to();
  }
}
