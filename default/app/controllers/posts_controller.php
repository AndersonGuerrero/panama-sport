<?php

class PostsController extends AppController
{
  public function index(){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Posts';
    $this->subtitulo = 'Listado';
    $this->url = 'posts';
    $post = new Posts();
    $this->listPosts = $post->getPosts();
  }

  public function create()
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Posts';
    $this->subtitulo = 'Crear';
    $this->url = 'posts';

    if(Input::hasPost('posts')){
      $data_post = array(
        'contenido' =>Input::post('posts')['contenido'],
        'titulo' =>Input::post('posts')['titulo'],
        'vistas' => 0,
        'categoriaspost_id' =>Input::post('posts')['categoriaspost_id']
      );
      if($_FILES){
        $target_path = FILES_PATH.'posts/';
        $target_path = $target_path.basename($_FILES['imagen']['name']);
        if(move_uploaded_file($_FILES['imagen']['tmp_name'], $target_path)){
          $data_post['imagen'] = $_FILES['imagen']['name'];
        }
      }

      $post = new Posts($data_post);

      if($post->create()){
        Flash::valid('Operación exitosa');
        Input::delete();
        return;
      }
      Flash::error('Fallo la operación');
    }
  }

  public function update($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    View::template('admin');
    $this->titulo = 'Posts';
    $this->subtitulo = 'Editar';
    $this->url = 'posts';

    $post = new Posts();
    if(Input::hasPost('post')){
      $data_post = array(
        'id' =>Input::post('post')['id'],
        'titulo' =>Input::post('post')['titulo'],
        'contenido' =>Input::post('post')['contenido'],
        'categoriaspost_id' =>Input::post('post')['categoriaspost_id']
      );
      if($_FILES){
        $target_path = FILES_PATH.'posts/';
        $target_path = $target_path.basename($_FILES['imagen']['name']);
        if(move_uploaded_file($_FILES['imagen']['tmp_name'], $target_path)){
          $data_post['imagen'] = $_FILES['imagen']['name'];
        }
      }
      if($post->update($data_post)){
        Flash::valid('Operación exitosa');
        return Redirect::to();
      }
      Flash::error('Fallo la operación');
      return;
    }
    $this->post = $post->find_by_id((int) $id);
  }

  public function delete($id)
  {
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    if ((new Posts)->delete((int) $id)) {
            Flash::valid('Operación exitosa');
    } else {
            Flash::error('Falló Operación');
    }
    return Redirect::to();
  }
}
