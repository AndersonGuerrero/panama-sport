<?php

class Noticias extends ActiveRecord
{
    public function getNoticias(){
      return $this->paginate("order: id desc");
    }

    public function getUltimasNoticias($page){
      return $this->paginate("page: 1", "per_page: 9","order: created_at desc");
    }

}
