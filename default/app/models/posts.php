<?php

class Posts extends ActiveRecord
{
    public function getPosts(){
      return $this->paginate("order: id desc");
    }

    public function getUltimosPosts($page){
      return $this->paginate("page: 1", "per_page: 5","order: created_at desc");
    }
}
