<?php

class Functions
{
  public function ordenarLista($lista) {
    $lista_ordenada = [];
    $tipo = 1;
    $count_items = 0;
    $count = 0;
    $items_list = [];
    foreach ($lista as $item){
      $count++;
      if($tipo==1){
        $lista_ordenada[] = $item;
        $tipo = 0;
      }else{
        $count_items++;
        $items_list[] = $item;
      }
      if($tipo==0 && $count_items==4){
        $lista_ordenada[] = $items_list;
        unset($items_list);
        $count_items = 0;
        $tipo = 1;
      }
      if(count($lista) == $count){
        $lista_ordenada[] = $items_list;
      }
    }
    return $lista_ordenada;
  }
}
