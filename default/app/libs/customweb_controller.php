<?php
/**
 * @see Controller nuevo controller
 */
require_once CORE_PATH . 'kumbia/controller.php';

/**
 * Controlador principal que heredan los controladores
 *
 * Todas las controladores heredan de esta clase en un nivel superior
 * por lo tanto los metodos aqui definidos estan disponibles para
 * cualquier controlador.
 *
 * @category Kumbia
 * @package Controller
 */
class CustomwebController extends Controller
{

    final protected function initialize()
    {
      // ------------ data panel de la derecha
      $this->listUltimosPosts = (new Posts())->getUltimosPosts(1);
      $this->listPostsPopulares = (new Posts())->find(
        "per_page: 10",
        "order: vistas desc"
      );
      $this->listLinks = (new Links())->find();
      $this->listCategoriaspost = (new Categoriaspost())->find();
      // ------------ -------------------

      $this->fecha = Fechacastellano::get();
      $this->noticias = (new Noticias())->find(
        "conditions: carrusel=0",
        "order: created_at desc"
      );
      $this->ultimasNoticias = [];
      $count = 1;
      foreach ($this->noticias as $key => $value) {
        $count++;
        if($count<=5){
          $this->ultimasNoticias[] = $value;
          unset($this->noticias[$key]);
        }else{
          break;
        }
      }
      $banners = (new Banners())->find();
      if($banners){
        $this->banner = $banners[mt_rand(0, count($banners)-1)];
      }else{
        $this->banner = FALSE;
      }
      $videos = (new Videos())->find();
      if($videos){
        $this->video = $videos[mt_rand(0, count($videos)-1)];
      }else{
        $this->video = FALSE;
      }
      $this->listCategorias = (new Categorias())->find();
    }

    final protected function finalize()
    {

    }

}
